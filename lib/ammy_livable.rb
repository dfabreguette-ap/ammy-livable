require "ammy_livable/version"

#encoding: utf-8
module AmmyLivable
  extend ActiveSupport::Concern

  included do

    field :is_online,          type: Mongoid::Boolean
    field :last_active_at,     type: Time

    scope :online, lambda {
      where(is_online: true).gte(last_active_at: (Time.now - 2.minute))
    }

    scope :timeout_online, lambda {
      where(is_online: true).lt(last_active_at: (Time.now - 2.minute))
    }

    def live_active!(is_online: true)
      if self.is_online != is_online or !last_active_at or (last_active_at and last_active_at < Time.now - 1.minute)
        self.set(
          is_online: is_online,
          last_active_at: Time.now
        )
        ammy_livable_option(:on_live_activity_changed)
      end
    end

    def self.expire_live_timeouts!
      User.timeout_online.each do |user|
        user.live_active!(is_online: false)
      end
    end

    def ammy_livable_option(key, options = {})
      # There's no "ammy_livable_options" method / variable defined if "ammy_livable()" method is not called on the model
      if self.class.respond_to? :ammy_livable_options and ammy_livable_option_key = self.class.ammy_livable_options[key.to_sym]

        if ammy_livable_option_key.is_a? Proc
          lambda_params = [self]
          if options[:lambda_extra_params]
            lambda_params += options[:lambda_extra_params]
          end
          option = ammy_livable_option_key.(*lambda_params)
        else
          # Check for static options (like icon)
          if self.class.static_options.include?(key.to_sym)
            option = ammy_livable_option_key
          else
            option = send(ammy_livable_option_key)
          end
        end
      end
      option
    end

  end


  module ClassMethods

    @@default_ammy_livable_options = {
      on_live_activity_changed: lambda{|user|
        # ...
      }
    }

    # Defines options that will not be sent to instance using ruby send method but used as static variable
    @@ammy_livable_static_options = []

    @@ammy_livable_required_options = []

    def ammy_livable(options = {})

      # create a reader on the class to access the options from the ammy_livable instance
      class << self; attr_reader :ammy_livable_options; end

      if missing_options = (@@ammy_livable_required_options - options.keys) and missing_options.present?
        raise "Missing Options #{missing_options} ! Please define those options to use this concern !"
      end

      @ammy_livable_options = @@default_ammy_livable_options.merge options

      if @ammy_livable_options[:trigger].present?
        send(@ammy_livable_options[:trigger], :notify!)
      end
    end

    def static_options
      @@static_options
    end
  end
end
