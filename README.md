# AmmyLivable

Ammy Livable allows to track if an Ammy User is online and the last active moment.

It provides a "AmmyLivable" concern that :
- adds the two attributes : "is_online:Boolean" and "last_active_at:Time"
- the "live_active!" method see https://gitlab.com/dfabreguette-ap/ammy-livable/blob/master/lib/ammy_livable.rb#L20 for signature
- a "on_live_activity_changed" callback called on every active change



## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ammy_livable', git: "git@gitlab.com:dfabreguette-ap/ammy-livable.git"
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ammy_livable

## Usage

Simply add the concern to the mongoid model and call the concern configure method with options.

```
  class User
    include AmmyLivable

    ammy_livable({
      on_live_activity_changed: -> (user) {
          # Called every time "is_online" changes
      }
    })
  end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/ammy_livable.
